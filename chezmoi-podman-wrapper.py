#!/usr/bin/env python3
"""
chezmoi-podman-wrapper.py

Description:
    This script runs Chezmoi, a dotfile manager, inside a Podman container to avoid installing it on systems that discourages installing applications to the OS, like Fedora Silverblue
    It allows running the container with arbitrary Chezmoi commands, as well as a with the '--test' option to use a local container image when testing new versions of it.

Usage:
    $ ./chezmoi-podman-wrapper.py [OPTIONS] [COMMANDS]

Options:
    -t, --test       Use a local image instead of pulling from the online registry.

    -a, --alias      Print the command that would otherwise be executed. For adding as an alias.

Commands:
    Specify Chezmoi commands to be passed to the container. If no command is provided, 'update' will be run by default.

Examples:
    1. Run Chezmoi update:
        $ ./chezmoi-podman-wrapper.py

    2. Run Chezmoi diff:
        $ ./chezmoi-podman-wrapper.py diff

    3. Use a local image for testing and run Chezmoi apply:
        $ ./chezmoi-podman-wrapper.py -t apply
"""

import argparse
import subprocess
import os
import logging

logging.basicConfig(format='%(levelname)s: %(message)s')

# Define constants for creating the command
PULL_POLICY = 'newer'
CHEZMOI_PATH = "/usr/bin/chezmoi"
HOME_MOUNT = f"{os.path.expandvars('$HOME')}:{os.path.expandvars('$HOME')}"
SSH_SOCK_MOUNT = f"{os.path.expandvars('$SSH_AUTH_SOCK')}:{os.path.expandvars('$SSH_AUTH_SOCK')}"
SSH_AUTH_SOCK = f"SSH_AUTH_SOCK={os.path.expandvars('$SSH_AUTH_SOCK')}"
SSH_AGENT_OPTS = ["--volume", SSH_SOCK_MOUNT,
                  "--env", SSH_AUTH_SOCK]

# Handle Arguments
parser = argparse.ArgumentParser(description='Runs Chezmoi in a Podman container.')
parser.add_argument('command', metavar='command', nargs='*',
                    default=['update'],
                    help='commands to pass to Chezmoi')
parser.add_argument('-t', '--test', dest='test', action='store_true',
                    help='use local image (default: pull image from online registry)')
parser.add_argument('-a', '--alias', dest='alias', action='store_true',
                    help='print the command that would be executed. For adding as an alias to "chezmoi"')
parser.add_argument('--disable-ssh-agent', dest='disable_ssh_agent', action='store_true',
                    help='manually disable configuration of SSH Agent. Suppresses warning if env vars are missing.')
args = parser.parse_args()

# Check if running in test mode
repo = None
if args.test:
    repo = "localhost/oci-chezmoi"
else:
    repo = "registry.gitlab.com/unshippedreminder/oci-chezmoi"

base_command = ["podman",
           "run",
           "--pull=" + PULL_POLICY,
           "--rm",
           "--userns=keep-id",
           "--volume", HOME_MOUNT,
           "--security-opt", "label=disable"]

# Check if $SSH_AUTH_SOCK is set and remove ssh-agent settings if not, or manually disabled.
if "SSH_AUTH_SOCK" not in os.environ and not args.disable_ssh_agent:
    logging.warning("$SSH_AUTH_SOCK is not defined on your system. Disabling SSH Agent settings...")
elif not args.disable_ssh_agent:
    base_command += SSH_AGENT_OPTS

COMMAND = base_command + [repo, CHEZMOI_PATH] + args.command

if args.alias:
    print(' '.join(COMMAND))
else:
    subprocess.run(COMMAND)
