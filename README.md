# OCI Chezmoi image

A minimal OCI image with only [chezmoi](https://www.chezmoi.io/) installed.

Runs `chezmoi apply` by default.

## Usage

See `./chezmoi-podman-wrapper.py --help`

## Included software

Following software is included in the image to allow for more than the most basic setups:

- git
- gpg
