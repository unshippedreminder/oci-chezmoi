#!/bin/bash

# Based on: https://github.com/containers/buildah/blob/main/docs/tutorials/01-intro.md#building-a-container-from-scratch

echo "Setting up..."
export container_name='oci-chezmoi'
export build_container=$(buildah from scratch)
export latest_release_binary='https://github.com/twpayne/chezmoi/releases/latest/download/chezmoi-linux-amd64-musl'
export binary_path='/usr/bin/chezmoi'
export releasever=$(rpm -E %fedora)

echo "Downloading Chezmoi binary..."
buildah copy --chmod 755 $build_container $latest_release_binary $binary_path

echo "Installing dependencies..."
buildah unshare
export image_mount_path=$(buildah mount $build_container)
dnf install --releasever $releasever --installroot $image_mount_path --setopt install_weak_deps=false -y openssh-clients git-core gnupg2 flatpak-spawn

echo "Configuring image..."
buildah config --cmd "$binary_path update" $build_container
buildah config --created-by "unshippedreminder"  $build_container
buildah config --author "https://gitlab.com/unshippedreminder/" --label name=$container_name $build_container

echo "Commiting image..."

if [ $1 = "test" ]; then
  ## Local Testing
  echo "Running in local test mode"
  buildah commit $build_container $container_name:latest
else
  ## Production Build
  buildah commit $build_container ${CI_PROJECT_NAME}:latest
fi
